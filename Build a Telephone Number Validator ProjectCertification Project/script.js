document.getElementById('check-btn').addEventListener('click', validatePhoneNumber);
document.getElementById('clear-btn').addEventListener('click', clearResults);

function validatePhoneNumber() {
  const userInput = document.getElementById('user-input').value;
  const resultsDiv = document.getElementById('results-div');

  if (!userInput.trim()) {
    alert('Please provide a phone number');
    return;
  }

  // Regular expression for validating US phone numbers
  const phoneRegex = /^1?[-.\s]?(\([2-9]\d{2}\)|[2-9]\d{2})[-.\s]?[2-9]\d{2}[-.\s]?\d{4}$/;

  if (phoneRegex.test(userInput)) {
    resultsDiv.innerText = `Valid US number: ${userInput}`;
  } else {
    resultsDiv.innerText = `Invalid US number: ${userInput}`;
  }
}

function clearResults() {
  document.getElementById('results-div').innerText = '';
}
