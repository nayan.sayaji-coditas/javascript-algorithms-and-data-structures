document.getElementById('convert-btn').addEventListener('click', function() {
    // Get the input value
    var inputNumber = document.getElementById('number').value;

    // Validate input
    if (inputNumber === '') {
        document.getElementById('output').innerText = 'Please enter a valid number';
    } else if (inputNumber < 1) {
        document.getElementById('output').innerText = 'Please enter a number greater than or equal to 1';
    } else if (inputNumber >= 4000) {
        document.getElementById('output').innerText = 'Please enter a number less than or equal to 3999';
    } else {
        // Convert the number to Roman numeral
        var romanNumeral = convertToRoman(inputNumber);
        document.getElementById('output').innerText = romanNumeral;
    }
});

function convertToRoman(num) {
    var romanNumeral = '';
    var romanValues = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    var romanSymbols = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];

    for (var i = 0; i < romanValues.length; i++) {
        while (num >= romanValues[i]) {
            romanNumeral += romanSymbols[i];
            num -= romanValues[i];
        }
    }

    return romanNumeral;
}
